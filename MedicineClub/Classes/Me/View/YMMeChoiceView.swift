//
//  YMMeChoiceView.swift
//  DanTang
//
//  Created by 杨蒙 on 16/7/24.
//  Copyright © 2016年 hrscy. All rights reserved.
//

import UIKit
import Kingfisher

class YMMeChoiceView: UIView {
    
    var table:UITableView
    
    override init(frame: CGRect) {
        self.table = UITableView(); 
        
        super.init(frame: frame)
        
        setupUI()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI() {
        addSubview(leftButton)
        addSubview(middleButton)
        addSubview(rightButton)
        
        addSubview(indicatorView)
        
        leftButton.snp.makeConstraints { (make) in
            make.top.left.bottom.equalTo(self)
            make.width.equalTo(rightButton)
        }
        
        middleButton.snp.makeConstraints { (make) in
            make.left.equalTo(leftButton.snp.right)
            make.top.bottom.equalTo(self)
            make.width.equalTo(rightButton)
        }
        
        rightButton.snp.makeConstraints { (make) in
            make.left.equalTo(middleButton.snp.right)
            make.top.right.bottom.equalTo(self)
        }
        
      
        
        indicatorView.snp.makeConstraints { (make) in
            make.height.equalTo(kIndicatorViewH)
            make.bottom.left.equalTo(self)
            make.right.equalTo(leftButton)
        }
    }
    
    /// 左边的按钮
    private lazy var leftButton: UIButton = {
        let leftButton = UIButton()
        leftButton.setTitle("Principal", for: .normal)
        leftButton.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        leftButton.setTitleColor(YMColor(r: 0, g: 0, b: 0, a: 0.7), for: .normal)
        leftButton.backgroundColor = UIColor.white
        leftButton.addTarget(self, action: #selector(leftButtonClick), for: .touchUpInside)
        leftButton.layer.borderColor = YMColor(r: 230, g: 230, b: 230, a: 1.0).cgColor
        leftButton.layer.borderWidth = klineWidth
        leftButton.isSelected = true
        return leftButton
    }()
    
    private lazy var middleButton: UIButton = {
        let middleButton = UIButton()
        middleButton.setTitle("Conversas", for: .normal)
        middleButton.setTitleColor(YMColor(r: 0, g: 0, b: 0, a: 0.7), for: .normal)
        middleButton.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        middleButton.backgroundColor = UIColor.white
        middleButton.addTarget(self, action: #selector(middleButtonClick), for: .touchUpInside)
        middleButton.layer.borderColor = YMColor(r: 230, g: 230, b: 230, a: 1.0).cgColor
        middleButton.layer.borderWidth = klineWidth
        return middleButton
    }()
    
    /// 右边的按钮
    private lazy var rightButton: UIButton = {
        let rightButton = UIButton()
        rightButton.setTitle("Contatos", for: .normal)
        rightButton.setTitleColor(YMColor(r: 0, g: 0, b: 0, a: 0.7), for: .normal)
        rightButton.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        rightButton.backgroundColor = UIColor.white
        rightButton.addTarget(self, action: #selector(rightButtonClick), for: .touchUpInside)
        rightButton.layer.borderColor = YMColor(r: 230, g: 230, b: 230, a: 1.0).cgColor
        rightButton.layer.borderWidth = klineWidth
        return rightButton
    }()
    
    /// 底部红色条
    private lazy var indicatorView: UIView = {
        let indicatorView = UIView()
        // indicatorView.backgroundColor = YMGlobalRedColor()
        
        //<color key="backgroundColor" red="0.35523228582271937" green="0.67631946561007816" blue="0.30289597361621529" alpha="1" colorSpace="custom" customColorSpace="sRGB"/>
        indicatorView.backgroundColor = UIColor(red:0.35523228582271937,green: 0.67631946561007816, blue:0.30289597361621529, alpha:1);
        return indicatorView
    }()
    
    @objc func leftButtonClick(button: UIButton) {
        button.isSelected = !button.isSelected
        UIView.animate(withDuration: kAnimationDuration) {
            self.indicatorView.x = 0
        }
        
        let webView = UIWebView();
        let url = URL(string: "http://www.google.com")
        let request = URLRequest(url: url! as URL)
        webView.frame = CGRect(x: 0, y: 0, width: SCREENW, height: SCREENH)
        webView.scalesPageToFit = true
        webView.dataDetectorTypes = .all
        webView.loadRequest(request)
        
        table.tableFooterView = webView;
    }
    
    @objc func middleButtonClick(button: UIButton) {
        button.isSelected = !button.isSelected
        UIView.animate(withDuration: kAnimationDuration) {
            self.indicatorView.x = SCREENW * 0.33333
        }
        let webView = UIWebView();
        let url = URL(string: "http://www.uol.com")
        let request = URLRequest(url: url! as URL)
        webView.frame = CGRect(x: 0, y: 0, width: SCREENW, height: SCREENH)
        webView.scalesPageToFit = true
        webView.dataDetectorTypes = .all
        
        
        webView.loadRequest(request)
        
        let message = YMMessageViewController()
        table.tableFooterView = message.view
    }
    
    @objc func rightButtonClick(button: UIButton) {
        button.isSelected = !button.isSelected
        UIView.animate(withDuration: kAnimationDuration) {
            self.indicatorView.x = SCREENW * 0.66666
        }
        
        
        table.tableFooterView = YMMeFooterView(frame: CGRect(x: 0, y: 0, width: SCREENW, height: 200))
    }
}
