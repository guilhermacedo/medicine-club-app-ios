//
//  YMMeViewController.swift
//  DanTang
//
//  Created by 杨蒙 on 16/7/19.
//  Copyright © 2016年 hrscy. All rights reserved.
//
//  我
//

import UIKit

class YMMeViewController: YMBaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    var cellCount = 0
    
    var tableView = UITableView();
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    /// 创建 tableView
    private func setupTableView() {
        tableView = UITableView()
        tableView.frame = view.bounds
        view.addSubview(tableView)
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableHeaderView = headerView
        tableView.tableFooterView = footerView
    }
    
    private lazy var headerView: YMMineHeaderView = {
        let headerView = YMMineHeaderView()
        headerView.frame = CGRect(x: 0, y: 0, width: SCREENW, height: kYMMineHeaderImageHeight)
        headerView.iconButton.addTarget(self, action: #selector(iconButtonClick), for: .touchUpInside)
        headerView.messageButton.addTarget(self, action: #selector(messageButtonClick), for: .touchUpInside)
        headerView.settingButton.addTarget(self, action: #selector(settingButtonClick), for: .touchUpInside)
        return headerView
    }()
    
    // MARK: - 头部按钮点击
    @objc func iconButtonClick() {
        let loginVC = YMDetailViewController()
        loginVC.homeItem = YMHomeItem(dict:["content_url":"http://www.google.com" as AnyObject]
            
        );
        // loginVC.homeItem?.content_url="http://www.google.com"
        loginVC.title = "Login"
        // let nav = YMNavigationController(rootViewController: loginVC)
        // present(loginVC, animated: true, completion: nil)
    }
    
    @objc func messageButtonClick() {
        print("called")
        let messageVC = YMMessageViewController()
        messageVC.title = "消息"
        // present(messageVC, animated: true, completion: nil)
    }
    
    @objc func settingButtonClick() {
        let settingVC = YMSettingViewController()
        settingVC.title = "更多"
        self.nav.pushViewController(settingVC, animated: true)
    }
    
    private lazy var footerView: UIView = {
        /*let footerView = YMMeFooterView(frame: CGRect(x: 0, y: 0, width: SCREENW, height: 200))
        return footerView*/
        
        let webView = UIWebView();
        let url = URL(string: "http://medicineclub.com.br")
        let request = URLRequest(url: url! as URL)
        webView.frame = CGRect(x: 0, y: 0, width: SCREENW, height: SCREENH)
        webView.scalesPageToFit = true
        webView.dataDetectorTypes = .all
        
        
        webView.loadRequest(request)
        
        return webView
    }()
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let choiceView = YMMeChoiceView()
        choiceView.table = self.tableView
        return choiceView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        if offsetY < 0 {
            var tempFrame = headerView.bgImageView.frame
            tempFrame.origin.y = offsetY
            tempFrame.size.height = kYMMineHeaderImageHeight - offsetY
            headerView.bgImageView.frame = tempFrame
        }
    }
}
