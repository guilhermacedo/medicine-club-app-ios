//
//  YMLoginViewController.swift
//  DanTang
//
//  Created by 杨蒙 on 16/7/20.
//  Copyright © 2016年 hrscy. All rights reserved.
//

import UIKit

class YMLoginViewController: YMBaseViewController, UITextFieldDelegate {
    /// 手机号
    @IBOutlet weak var mobileField: UITextField!
    /// 密码
    @IBOutlet weak var passwordField: UITextField!
    /// 登录按钮
    @IBOutlet weak var loginButton: UIButton!
    /// 忘记密码按钮
    @IBOutlet weak var forgetPwdBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // mobileField.becomeFirstResponder()
        mobileField.delegate = self
        passwordField.delegate = self
    }
    
    @IBAction func makeLogin(_ sender: UIButton) {
        self.present(YMMeViewController(), animated: false, completion: nil)
    
    }
    
    @IBAction func doClick(_ sender: UITapGestureRecognizer){
        mobileField.resignFirstResponder()
        passwordField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == mobileField{
            passwordField.becomeFirstResponder();            
        }
        
        if textField == passwordField {
            passwordField.resignFirstResponder()
        }
        
        return true
    }
    
    
}
